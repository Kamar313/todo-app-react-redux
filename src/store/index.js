import { legacy_createStore } from "redux";
import reduser from "./reducer/items";
const store = legacy_createStore(
  reduser,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
export default store;
store.subscribe(() => {
  console.log(store.getState());
});
